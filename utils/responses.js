class Responses {

    success(req, res, data, status) {

        let statusCode = status || 200;

        res.status(statusCode).send({
            error: null,
            success: true,
            data
        })
    }

    error(req, res, error, status) {
        let statusCode = status || 500;

        res.status(statusCode).send({
            error,
            success: false,
            data: null
        })
    }
}

module.exports = new Responses();