const stadistic = require('../../server/models/stadistics');
const seguimiento = require('../../server/models/seguimiento');


class stadisticStudent {

    async saveStadisticsByDay() {

        const courses = await seguimiento.distinct("course");
        console.log("saving sadistics by day");
        let count = 0;
        for await (let course of courses) {
            const students = await seguimiento.find({ course: course }).distinct('username');

            for await (let student of students) {

                const days = await seguimiento.find({ "username": student, "course": course }).distinct('date');

                for await (let day of days) {

                    const stadisticToSave = await getBasicStadisticByDay(course, student, day);
                    const stadisticToSaveObj = { student, course, data: stadisticToSave, control: day, _id: student + course + '_' + day, controlDS: 'day' };

                    const indicatorsaved = await saveStadistics(stadisticToSaveObj);
                    console.log(course, student);
                }

            }
            count++;
            const percent = (count / courses.length) * 100;
            console.log(percent);
        }
    }

    async saveStadisticsBySession() {

        const courses = await seguimiento.distinct("course");
        let count = 0;
        let count2 = 0;
        console.log("saving sadistics by session");

        for await (let course of courses) {
            const students = await seguimiento.find({ course: course }).distinct('username');

            for await (let student of students) {

                const sessions = await seguimiento.find({ "username": student, "course": course }).distinct('session');

                for await (let session of sessions) {

                    const stadisticToSave = await getBasicStadisticSession(course, student, session);
                    const stadisticToSaveObj = { student, course, data: stadisticToSave, control: session, _id: student + course + '_' + session, controlDS: 'session' };

                    const indicatorsaved = await saveStadistics(stadisticToSaveObj);

                }
                count2++;
                const percent2 = (count2 / (courses.length + students.length + sessions.length)) * 100;
                console.log(percent2);

            }
            count2 = 0;
            count++;
            const percent = (count / courses.length) * 100;
            console.log(percent, "%");
        }
    }
}

//  ===== By Day =====
function getBasicStadisticByDay(course, student, day) {

    return new Promise(async (resolve, reject) => {

        const Videos = await seguimiento.find(
            {
                $and: [
                    { "username": student, "course": course, "date": day },
                    { $or: [{ name: "play_video" }, { name: "pause_video" }, { name: "stop_video" }] }
                ]
            }).countDocuments()


        const Contenido = await seguimiento.find({
            $and: [
                { $or: [{ name: "nav_content" }, { name: "nav_content_click" }, { name: "nav_content_prev" }, { name: "nav_content_next" }, { name: "nav_content_tab" }] },
                { "username": student, "course": course, "date": day }
            ]
        }).countDocuments();

        const Foros = await seguimiento.find({
            $and: [
                { $or: [{ name: "edx.forum.comment.created" }, { name: "edx.forum.response.created" }, { name: "edx.forum.thread.created" }] },
                { "username": student, "course": course, "date": day }
            ]
        }).countDocuments();

        const Examenes = (await seguimiento.find({
            $and: [
                // , { name: "problem_graded" }
                { $or: [{ name: "problem_check" }] },
                { "username": student, "course": course, "date": day }
            ]
        }).distinct('session')).length

        const Home = await seguimiento.find({
            $and: [
                { $or: [{ name: "Signin" }] },
                { "username": student, "course": course, "date": day }
            ]
        }).countDocuments();

        resolve({ Contenido, Videos, Home, Foros, Examenes })
    })
}

// ===== By Session =====
function getBasicStadisticSession(course, student, session) {

    return new Promise(async (resolve, reject) => {

        const Videos = await seguimiento.find(
            {
                $and: [
                    { "username": student, "course": course, "session": session },
                    { $or: [{ name: "play_video" }, { name: "pause_video" }, { name: "stop_video" }] }
                ]
            }).countDocuments()


        const Contenido = await seguimiento.find({
            $and: [
                { $or: [{ name: "nav_content" }, { name: "nav_content_click" }, { name: "nav_content_prev" }, { name: "nav_content_next" }, { name: "nav_content_tab" }] },
                { "username": student, "course": course, "session": session }
            ]
        }).countDocuments();

        const Foros = await seguimiento.find({
            $and: [
                { $or: [{ name: "edx.forum.comment.created" }, { name: "edx.forum.response.created" }, { name: "edx.forum.thread.created" }] },
                { "username": student, "course": course, "session": session }
            ]
        }).countDocuments();

        const Examenes = (await seguimiento.find({
            $and: [
                // , { name: "problem_graded" }
                { $or: [{ name: "problem_check" }] },
                { "username": student, "course": course, "session": session }
            ]
        }).distinct('session')).length

        const Home = await seguimiento.find({
            $and: [
                { $or: [{ name: "Signin" }] },
                { "username": student, "course": course, "session": session }
            ]
        }).countDocuments();

        resolve({ Contenido, Videos, Home, Foros, Examenes })
    })

}


// ==== save statdistic =====
async function saveStadistics(stadisticToSaveObj) {

    const conditions = { _id: stadisticToSaveObj._id }
        , update = stadisticToSaveObj
        , options = { multi: true, upsert: true };

    return stadistic.updateOne(conditions, update, options);
}
module.exports = stadisticStudent;