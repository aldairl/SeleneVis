const express = require('express');
const bodyParser = require('body-parser');

const config = require('../config');

const stadisticStudent = require('./services/saveStadiscticStudent');
const stadisticStudentService = new stadisticStudent();

require('../server/database');
const cron = require('node-cron');

const app = express();
app.use(bodyParser.json());

app.listen(config.stadisticStudent.port, () => {
    console.log("service stadistic student on port", config.stadisticStudent.port);
    stadisticStudentService.saveStadisticsByDay();
});

cron.schedule('30 7 * * *', () => {
    stadisticStudentService.saveStadisticsByDay();
});

cron.schedule('10 8 * * *', () => {
    stadisticStudentService.saveStadisticsBySession();
});