const express = require('express');
const bodyParser = require('body-parser');

const contrllerGraphs = require('./controllers/grafos.controller')
const config = require('../config');
const routesGraphsDay = require('./routes/grafos.routes');
const graphsDayService = require('./services/saveGraphsDay');

require('../server/database');
const cron = require('node-cron');

const app = express();
app.use(bodyParser.json());

app.use('/api/graphs-day', routesGraphsDay);

app.listen(config.graphsDay.port, () => {
    console.log("service graphs Day on port", config.graphsDay.port);
});

cron.schedule('00 05 * * *', () => {
    graphsDayService.saveAllGraphsByDay();
});