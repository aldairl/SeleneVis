const seguimiento = require('../../server/models/seguimiento');
const grafosModel = require('../../server/models/grafos.model');
const colors = require('../../server/util/colorsGraps');

class GraphsByDay {

    async saveAllGraphsByDay() {

        // get all courses :P
        const courses = await seguimiento.distinct("course");

        console.log("updating grafos by day...");

        // for await (let course of courses) {

        for (let indexCourse = 0; indexCourse < courses.length; indexCourse++) {
            const course = courses[indexCourse];

            // }
            const students = await seguimiento.find({ course: course }).distinct('username');
            // for await (let student of students) {

            for (let indexStudent = 0; indexStudent < students.length; indexStudent++) {
                const student = students[indexStudent];
                // }
                const body = { student, course };
                // const body = { student: "Miguel_Palta_Castrillon", course: "Unicauca+LeanStartUp+2020-I" }

                const grafoToSave = await this.buildGraphsStudent(body);
                console.log(student, course);

                const saved = await saveGraphOnDB({ ...grafoToSave, control: "day" });
            }
            // console.log(course);
        }

        console.log("update finish")
    }

    async buildGraphsStudent(body) {
        return new Promise((resolve, reject) => {

            const { course, student } = body;

            Promise.all([
                getActivities(student, course),
                getAllNodes(student, course),
                getDays(student, course)
            ]).then(responses => {

                const activities = responses[0];
                const nodes = responses[1];
                const days = responses[2];

                buildEdges(activities, nodes, days, 'day').then(nodesTotal => {

                    resolve(
                        { student, course, edges: nodesTotal, nodes, options: days }
                    )

                })

            })
        })
    }


    async saveGraphsProgramer() {
        exectTask(this.saveAllGraphsByDay());
    }

}


function exectTask(task) {
    setTimeout(function () { task }, 1500);
}

function getActivities(student, course) {

    return new Promise((resolve, reject) => {
        seguimiento.find({ "username": student, "course": course }, 'session name date time')
            .sort('date_time')
            .exec((err, activities) => {

                if (err) {
                    reject("error", err);
                } else {
                    agrupingActivities(activities).then(() => {
                        resolve(activities)
                    })
                }
            })
    })
}

function getDays(student, course) {

    return new Promise(async (resolve, reject) => {

        let daysProcessed = [];
        //all days ['2020-01-10']
        const days = await seguimiento.find({ "username": student, "course": course })
            .sort('date_time')
            .distinct('date');
        // all date with exams
        const daysExams = await seguimiento.find({ "username": student, "course": course, name: 'problem_check' })
            .sort('date_time')
            .distinct('date');

        if (!days.length) resolve([]);

        for (let indexDay = 0; indexDay < days.length; indexDay++) {
            const day = days[indexDay];

            let dayObject = { control: day }

            if (daysExams.length) {

                for (let indexDayExam = 0; indexDayExam < daysExams.length; indexDayExam++) {
                    const dayExam = daysExams[indexDayExam];

                    if (day == dayExam) {
                        dayObject.label = 'examen'
                    }
                }

            }

            daysProcessed.push(dayObject)

        }

        resolve(daysProcessed)
    })
}

function getAllNodes(student, course) {

    return new Promise((resolve, reject) => {
        seguimiento.find({ "username": student, "course": course }).distinct('name')
            .exec((err, nodes) => {

                if (err) {
                    reject("error", err);
                } else {
                    agoupingNodes(nodes).then((newNodes) => resolve(newNodes));
                }
            })
    })
}

function buildEdges(activities, nodes, days, control) {

    let nodesTotal = [];
    let countActivities = 0;
    // console.log("contruyendo grafo...", control, activities)

    return new Promise(async (resolve, reject) => {
        if (!activities.length) resolve([]);

        // days.forEach(async (dayObj, indexDay) => {

        for (let indexDay = 0; indexDay < days.length; indexDay++) {
            const dayObj = days[indexDay];

            // }
            const day = dayObj.control;
            // console.log("este es una actividad", activities[1], Object.keys(activities[1]))

            let activitiesByControl = [];
            if (control === 'day') {
                activitiesByControl = await activities.filter(activity => activity._doc.date === day);
            } else {
                activitiesByControl = await activities.filter(activity => activity._doc.session === day);
            }

            let nodesByDay = [];
            let nodesByDayOrder = [];
            // console.log("total actividades en ", day, activitiesByControl.length);
            let countOrder = 1;
            let secRepeat = false;

            if (activitiesByControl.length) {

                activitiesByControl = activitiesByControl.filter(activitity => activitity._doc.name !== "problem_graded");

                for (let index = 0; index < activitiesByControl.length; index++) {

                    countActivities++;
                    const element = nodes.find(obj => obj.label === activitiesByControl[index]._doc.name);

                    if (element) {

                        // if exist more than one register
                        if (!nodesByDay.length) {

                            nodesByDay.push({
                                id: '-11',
                                to: element.id,
                                nameto: element.label,
                                from: -1,
                                namefrom: 'Inicio',
                                label: '1',
                                visits: 1
                            })

                            nodesByDayOrder.push({
                                id: '-11',
                                to: element.id,
                                nameto: element.label,
                                from: -1,
                                namefrom: 'Inicio',
                                label: countOrder.toString(),
                                visits: countOrder,
                                length: 200
                            })
                        }
                        if ((index + 1) <= (activitiesByControl.length - 1) && nodes.length) {

                            const elementnext = nodes.find(obj => obj.label === activitiesByControl[index + 1]._doc.name);
                            if (element && element.id && elementnext && elementnext.id) {

                                // const edge = {

                                //     id: `${element.id}${elementnext.id}`,
                                //     to: elementnext.id,
                                //     nameto: elementnext.label,
                                //     from: element.id,
                                //     namefrom: element.label,
                                //     label: '1',
                                //     visits: 1
                                // }

                                let edgeOrder = {
                                    id: `${element.id}${elementnext.id}`,
                                    idOrder: `${element.id}${elementnext.id}`,
                                    to: elementnext.id,
                                    nameto: elementnext.label,
                                    from: element.id,
                                    namefrom: element.label,
                                    label: (countOrder + 1).toString(),
                                    visits: countOrder + 1,
                                    length: 350
                                }

                                // if (!nodeWasAdded(edge.id, nodesByDay)) {

                                //     nodesByDay.push(edge);
                                // }

                                if (!nodeWasAddedOrder(edgeOrder, nodesByDayOrder)) {

                                    let lastEdgeAdded = nodesByDayOrder[(nodesByDayOrder.length - 1)];
                                    // console.log(day, lastEdgeAdded);

                                    if (edgeOrder.from !== lastEdgeAdded.to) {
                                        // is necessary create a scarcity node
                                        const idnodechange = `${lastEdgeAdded.to}${edgeOrder.from}`;

                                        const nodeIn = nodesByDayOrder.find(objNode => objNode.idOrder == idnodechange);

                                        if (nodeIn) {
                                            nodeIn.label = nodeIn.label + ', ' + edgeOrder.label;
                                            countOrder++;
                                            edgeOrder.label = (countOrder + 1).toString()
                                        } else {

                                            const edgeToAdd = {
                                                id: idnodechange,
                                                idOrder: idnodechange,
                                                to: edgeOrder.from,
                                                nameto: edgeOrder.label,
                                                from: lastEdgeAdded.to,
                                                namefrom: lastEdgeAdded.label,
                                                label: (countOrder + 1).toString(),
                                                visits: countOrder + 1,
                                                length: 350
                                            }

                                            nodesByDayOrder.push(edgeToAdd);
                                            countOrder++;

                                            edgeOrder.label = (countOrder + 1).toString();
                                        }

                                    }

                                    edgeOrder.id = edgeOrder.id + countOrder;
                                    nodesByDayOrder.push(edgeOrder);
                                    countOrder++;

                                } else {

                                }
                            } else {

                            }

                        } else {

                            let lastEdgeAdded = nodesByDayOrder[(nodesByDayOrder.length - 1)];

                            // const edge = {

                            //     id: `${element.id}-2`,
                            //     to: -2,
                            //     nameto: 'Fin',
                            //     from: element.id,
                            //     namefrom: element.label,
                            //     label: '1',
                            //     visits: 1
                            // }

                            const edgeOrder = {

                                id: `${lastEdgeAdded.to}-2`,
                                idOrder: `${lastEdgeAdded.to}-2`,
                                to: -2,
                                nameto: 'Fin',
                                // from: element.id,
                                from: lastEdgeAdded.to,
                                namefrom: element.label,
                                label: (countOrder + 1).toString(),
                                visits: countOrder + 1,
                                length: 300

                            }
                            // nodesByDay.push(edge);
                            nodesByDayOrder.push(edgeOrder);

                        }

                    } else {
                        console.log("not found", activitiesByControl[index]._doc.name);
                    }
                }

                // if (index === (activitiesByControl.length - 1)) {
                nodesTotal.push({ day: day, nodes: nodesByDay, nodesOrder: nodesByDayOrder });
                // }

                // console.log("se reaolvio", indexDay, days.length,  "num avtivities", countActivities, activities.length);
                // if (indexDay === (days.length - 1) && countActivities === (activities.length)) {
                // resolve(nodesTotal);
                // }

            } else {
                if (indexDay === (days.length - 1))
                    resolve(nodesTotal);
            }
            // });
        }


        resolve(nodesTotal);
    })

}

async function saveGraphOnDB(graph) {

    const _id = graph.student + graph.course + '_' + graph.control;
    const conditions = { _id }
        , update = { ...graph, _id }
        , options = { multi: true, upsert: true };

    const updateGraph = await grafosModel.updateOne(conditions, update, options);
    return updateGraph;
}

function agrupingActivities(activities) {

    return new Promise((resolve, reject) => {

        if (!activities.length) resolve();

        let count = 0;
        let newArrayActivities = [];
        for (let activity of activities) {
            // activity = {name date time}
            count++;
            if (activity._doc.name === "nav_content" || activity._doc.name === "nav_content_prev" || activity._doc.name === "nav_content_click"
                || activity._doc.name === "nav_content_next" || activity._doc.name === "nav_content_tab") {
                activity._doc.name = 'contenidos'

            } else if (activity._doc.name === "stop_video" || activity._doc.name === "pause_video" || activity._doc.name === "play_video") {
                activity._doc.name = 'videos';

            } else if (activity._doc.name === "Signin") {
                activity._doc.name = 'Home';

                // activity._doc.name === "problem_graded" || 
            } else if (activity._doc.name === "problem_check") {
                activity._doc.name = "examenes";

            } else if (activity._doc.name === "edx.forum.comment.created" || activity._doc.name === "edx.forum.response.created" || activity._doc.name === "edx.forum.thread.created") {
                activity._doc.name = 'foros';

            }

            // newArrayActivities.push(activity)
            if (count == (activities.length)) {
                resolve()
            }
        }
    })
}

function agoupingNodes(nodes) {

    let count = 0;
    let activityByNode = {
        contenidos: [],
        videos: [],
        Home: [],
        examenes: [],
        foros: []
    }
    return new Promise((resolve, reject) => {

        if (nodes.length) {

            // nodes = nodes.filter((el, index) => nodes.indexOf(el) === index);

            let nodesFilter = []
            // nodes = nodes.map((name) => {
            for (let indexName = 0; indexName < nodes.length; indexName++) {
                let name = nodes[indexName];

                // }
                // return { id: index, label: name }
                if (name !== "problem_graded") {

                    if (name === "nav_content" || name === "nav_content_prev" || name === "nav_content_click" || name === "nav_content_next" || name === "nav_content_tab") {
                        activityByNode.contenidos.push(name);
                        name = 'contenidos';
                    } else if (name === "stop_video" || name === "pause_video" || name === "play_video") {
                        activityByNode.videos.push(name);
                        name = 'videos';
                    } else if (name === 'Signin') {
                        activityByNode.Home.push(name);
                        name = 'Home'

                        // name === "problem_graded" || 
                    } else if (name === "problem_check") {
                        activityByNode.examenes.push(name);
                        name = "examenes";

                    } else if (name === "edx.forum.comment.created" || name === "edx.forum.response.created" || name === "edx.forum.thread.created") {
                        activityByNode.foros.push(name);
                        name = 'foros';
                    } else {
                        name = null
                    }
                    count++;
                    nodesFilter.push(name)
                    // return name;
                    // })
                }
            }

            nodes = nodesFilter;
            if (nodes.length) {
                // console.log(nodes)

                nodes = nodes.filter((el, index) => nodes.indexOf(el) === index);
                let count2 = 0;
                let newNodes = [{ id: -1, label: 'Inicio', color: `#64DD17` }]

                for (let name of nodes) {
                    activityByNode[name] = activityByNode[name].filter((el, index) => activityByNode[name].indexOf(el) === index);
                    const title = activityByNode[name].join(', ')
                    count2++;
                    newNodes.push({ id: count2, label: name, title: title, color: getColorByActivity(name) })
                }

                if (count2 == nodes.length) {
                    newNodes.push({ id: -2, label: '  Fin  ', color: `#D32F2F` });
                    // console.log(count, nodes.length, activityByNode, "nodes", newNodes);

                    resolve(newNodes);;
                }
            }
        } else {
            resolve([])
        }
    })
}

function getColorByActivity(nameActivity) {
    return colors[nameActivity];
}
// function nodeWasAddedOrder(idnode, nodes) {
function nodeWasAddedOrder(node, nodes) {

    const lengthnodes = nodes.length - 1;

    // console.log(lengthnodes, nodes)
    if (lengthnodes) {

        const itemOnArray = nodes.find(nodese => nodese.idOrder === node.idOrder);
        const itemOnArraysec = nodes[lengthnodes].idOrder === node.idOrder ? true : false;
        if (itemOnArray) {
            return true;
        } else {
            return false
        }
    } else {
        return false;
    }
}
module.exports = new GraphsByDay();