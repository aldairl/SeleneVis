const express = require('express');
const router =express.Router();
const grafos = require('../controllers/grafos.controller');

router.post('/', grafos.getGrafosStudentByDaySaved);
router.post('/sessions', grafos.getGrafosStudentBySessionSaved);
router.post('/create/session', grafos.createGrafosStudentBySession);
router.post('/create', grafos.createGraphsProgrammer);

module.exports = router;