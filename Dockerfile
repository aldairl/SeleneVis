FROM node:14-alpine

RUN mkdir -p /home/selene/app/node_modules && chown -R node:node /home/selene/app

WORKDIR /home/selene/app

COPY package*.json ./

USER node

RUN npm install

COPY --chown=node:node . .

EXPOSE 8080

CMD [ "node", "server/index.js" ]