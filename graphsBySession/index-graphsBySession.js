const express = require('express');
const bodyParser = require('body-parser');

const config = require('../config');

const graphsSessionService = require('./services/saveGraphsSession');

const database = require('../server/database');

const cron = require('node-cron');

const app = express();
app.use(bodyParser.json());

app.listen(config.graphsSession.port, () => {
    console.log("service graphs session on port", config.graphsSession.port);
});

cron.schedule('00 06 * * *', () => {
    graphsSessionService.saveAllGraphsBySession();
});