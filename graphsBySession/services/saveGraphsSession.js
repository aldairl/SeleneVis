const seguimiento = require('../../server/models/seguimiento');
const grafosController = require('../../graphsByDay/controllers/grafos.controller');

class GraphsBySession {

    async saveAllGraphsBySession() {

        // get all courses :P
        const courses = await seguimiento.distinct("course");

        console.log("updating grafos session...");

        // for await (let course of courses) {
        for (let indexCourse = 0; indexCourse < courses.length; indexCourse++) {

            const course = courses[indexCourse];
            const students = await seguimiento.find({ course: course }).distinct('username');

            for (let indexStudent = 0; indexStudent < students.length; indexStudent++) {

                // for await (let student of students) {
                const student = students[indexStudent];

                const body = { student, course };

                const grafoToSave = await grafosController.getGrafosStudentBySession(body);
                console.log(student, course);
                const saved = await grafosController.createGrafo({ ...grafoToSave, control: "session" });
            }
        }

        console.log("update finish")
    }


    async saveGraphsProgramer(){
        exectTask(this.saveAllGraphsBySession());
    }

}

function exectTask(task) {
    setTimeout(function () { task }, 1500);
}

module.exports = new GraphsBySession();