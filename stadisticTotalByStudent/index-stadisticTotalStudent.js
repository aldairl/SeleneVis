const express = require('express');
const bodyParser = require('body-parser');

const config = require('../config');

const stadisticstotalStudent = require('./services/saveStadiscticsTotalStudent');
const stadisticstotalStudentService = new stadisticstotalStudent();

const database = require('../server/database');

const cron = require('node-cron');

const app = express();
app.use(bodyParser.json());

app.listen(config.stadisticTotalStudent.port, () => {
    console.log("service stadistics Total on port", config.stadisticTotalStudent.port);
});

cron.schedule('40 7 * * *', () => {
    stadisticstotalStudentService.saveStatdisticTotalStudent();
});