const seguimiento = require('../../server/models/seguimiento');
const statics = require('./stadisticService');
const indicatorController = require('../../server/controllers/indicators.controller');

class StadisticTotalStudent {

    async saveStatdisticTotalStudent() {

        // get all courses :P
        const courses = await seguimiento.distinct("course");

        let docsInserteds = 0;
        let totalStudents = 0;

        console.log("updating...");
        // courses.forEach(async (course) => {
        for (let indexCourse = 0; indexCourse < courses.length; indexCourse++) {
            const course = courses[indexCourse];
            // }

            const students = await seguimiento.find({ course: course }).distinct('username');
            totalStudents += students.length;

            console.log(course);
            // students.forEach(async (student) => {
            for (let indexStudent = 0; indexStudent < students.length; indexStudent++) {
                const student = students[indexStudent];

                // }

                const staticsToSave = await statics.getStatistics({ course: course, student: student });

                // intent save document
                await indicatorController.createIndicator(staticsToSave)
                    .then(res => {
                        docsInserteds++;
                        console.log(docsInserteds, totalStudents)
                    })
                    .catch((err) => {
                        console.log("ocurrio un problema guardando el doc", err)
                    })

                if (docsInserteds === totalStudents) {
                    console.log(`updated ${docsInserteds} documents`)
                }
                // })
            }

            // })
        }
    }
}

module.exports = StadisticTotalStudent;