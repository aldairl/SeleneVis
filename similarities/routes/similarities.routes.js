const express = require('express');
const similarityRouter = express.Router();
const SimilarityController = require('../controllers/similarity.controller');

similarityRouter.post('/', SimilarityController.getSimilarity);
similarityRouter.post('/usernames', SimilarityController.getSimilarityByUsernames);
similarityRouter.post('/usernames/final', SimilarityController.getSimilarityFinalExamnByUsernames);


module.exports = similarityRouter;