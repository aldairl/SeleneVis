const responses = require('../../utils/responses');
const similarityService = require('../services/similarity.service');

class SimilarityController {

    async getSimilarity(req, res) {

        try {

            const { activityName, course, date, username } = req.body;

            const similarity = await similarityService.getSimilarityByActivityName(activityName, course, date, username);
            responses.success(req, res, similarity);

        } catch (error) {
            responses.error(req, res, error)
        }

    }

    async getSimilarityByUsernames(req, res) {

        try {

            const { activityName, course, date1, date2, username1, username2, seccion } = req.body;

            const similarity = await similarityService.getSimilarityByUsernames(activityName, course, date1, date2, username1, username2, seccion);
            responses.success(req, res, similarity);

        } catch (error) {
            responses.error(req, res, error)
        }

    }

    async getSimilarityFinalExamnByUsernames(req, res) {

        try {

            const { activityName, course, date1, date2, username1, username2, seccion } = req.body;

            const similarity = await similarityService.getSimilarityFinalExamByUsernames(activityName, course, date1, date2, username1, username2, seccion);
            responses.success(req, res, similarity);

        } catch (error) {
            responses.error(req, res, error)
        }

    }
}

module.exports = new SimilarityController();