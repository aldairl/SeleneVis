const express = require('express');
const bodyParser = require('body-parser');

const config = require('../config');

const database = require('../server/database');

const cron = require('node-cron');

const app = express();
app.use(bodyParser.json());

// routes
const similarityRoutes = require('./routes/similarities.routes');

app.use('/api/similarity', similarityRoutes);

app.listen(config.similarities.port, () => {
    console.log("service similarities on port", config.similarities.port);
});

/* cron.schedule('00 7 * * *', () => {
    graphsSessionService.saveAllGraphsBySession();
}); */