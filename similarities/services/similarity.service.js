const seguimientoModel = require('../models/seguimiento.model');

class SimilarityService {

    async getSimilarityByActivityName(activityName, course, date, username) {

        if (!activityName) activityName = 'problem_graded';
        if (!course) throw "course is required";
        if (!date) throw "date is required YY-MM-DD";
        if (!username) throw "username is required";

        let register = await seguimientoModel.find({ name: activityName, course, date, username })

        return [register[register.length - 1]];
    }

    async getSimilarityByUsernames(activityName, course, date1, date2, username1, username2, seccion) {

        const dataUsers = await getDataToSimilarityProcess(activityName, course, date1, date2, username1, username2, seccion);

        const data1 = dataUsers[0];
        const data2 = dataUsers[1];

        const response = await getSimilarityTwoUsers(data1, data2);

        return response

    }

    async getSimilarityFinalExamByUsernames(activityName, course, date1, date2, username1, username2, seccion) {

        const dataUsers = await getDataToSimilarityProcess(activityName, course, date1, date2, username1, username2, seccion);

        const data1 = dataUsers[0];
        const data2 = dataUsers[1];

        const response = await getSimilarityFinalExamTwoUsers(data1, data2);

        return response

    }

}

async function getSimilarityTwoUsers(_dataUser1, _dataUser2) {

    // data should be an array
    if (!_dataUser1.length) throw "data user 1 not exist"
    if (!_dataUser2.length) throw "data user 2 not exist"

    const dataUser1 = [_dataUser1[_dataUser1.length - 1]];
    const dataUser2 = [_dataUser2[_dataUser2.length - 1]];

    let questions1;
    let questions2;

    let similarities = [];
    let differents = [];

    for (let indexdata1 = 0; indexdata1 < dataUser1.length; indexdata1++) {

        questions1 = dataUser1[indexdata1]._doc.answers.split('&');
        questions2 = dataUser2[indexdata1]._doc.answers.split('&');

        for (let indexQuestion = 0; indexQuestion < questions1.length; indexQuestion++) {
            const questionsUser1 = questions1[indexQuestion];
            const questionsUser2 = questions2[indexQuestion];

            if (questionsUser1 === questionsUser2) {
                similarities.push({ questionsUser1, indexQuestion, indexdata1 });
            }
            // else {
            //     differents.push({ user1: questions1, user2: questions2, indexdata1 })
            // }
        }
    }

    const diffTime = await createFormatDifferenceTime(dataUser1[dataUser1.length - 1]._doc.date_time, dataUser2[dataUser2.length - 1]._doc.date_time)

    return {
        diffTime,
        responses: questions1.length,
        similarities: similarities.length / dataUser1.length,
        percentSimilarity: (similarities.length * 100) / (questions1.length * dataUser1.length),
        similaritiesQuestions: similarities,
    };

}

async function getSimilarityFinalExamTwoUsers(dataUser1, dataUser2) {

    // data should be an array
    if (!dataUser1.length) throw "data user 1 not exist"
    if (!dataUser2.length) throw "data user 2 not exist"

    let TotalSimilarities = [];

    let indexSimilarities1 = [];

    let answersUser1 = [];
    let answersUser2 = [];


    let answersOnly1 = [];

    // console.log(dataUser1, dataUser1.length);

    // ====== get answer users ====
    for (let indexdata1 = 0; indexdata1 < dataUser1.length; indexdata1++) {

        const answersUser1Obj = { answers: dataUser1[indexdata1]._doc.answers.split('&'), index: indexdata1 };
        const answersUser2Obj = { answers: dataUser2[indexdata1]._doc.answers.split('&'), index: indexdata1 };

        answersUser1.push(answersUser1Obj);
        answersUser2.push(answersUser2Obj);
    }

    // ==== answers similaritiesUser1 user 1 ======
    for (let indexAnswer = 0; indexAnswer < answersUser1.length; indexAnswer++) {
        const subAnswerUser1 = answersUser1[indexAnswer].answers;
        const subAnswerUser2 = answersUser2[indexAnswer].answers;
        let answersEq1 = [];
        let similaritiesUser1 = [];

        let answersEq2 = [];

        for (let indexSubAnswer = 0; indexSubAnswer < subAnswerUser1.length; indexSubAnswer++) {
            const subSubAnswerUser1 = subAnswerUser1[indexSubAnswer];


            for (let indexAnswerUser2 = 0; indexAnswerUser2 < answersUser2.length; indexAnswerUser2++) {
                const answerUser2 = answersUser2[indexAnswerUser2].answers;

                // search answes 
                const searchAnswer = answerUser2.find(answer => answer === subSubAnswerUser1);

                const searchOnlyAnswer = answerUser2.find(answer => answer.split('=')[0] === subSubAnswerUser1.split('=')[0]);

                if (searchAnswer) {
                    // console.log("array", answerUser2, "comparar", subSubAnswerUser1);
                    similaritiesUser1.push({ subSubAnswerUser1, indexAnswer });
                    indexSimilarities1.push(indexAnswer);
                }

                if (searchOnlyAnswer) {
                    // answersOnly1.push(subAnswerUser1);
                    answersEq1.push(subAnswerUser1);
                }
            }
        }

        if (answersEq1.length) {
            answersOnly1.push({ question: indexAnswer, anwers: answersEq1 })
        }

        if (similaritiesUser1.length) {
            TotalSimilarities.push({ question: indexAnswer, anwers: similaritiesUser1 })
        }

    }

    return {
        totalQuestions: dataUser2.length,
        questionsEquals: answersOnly1.length,
        equalAnswers: TotalSimilarities.length,
        percentSimilarityQuestions: (answersOnly1.length * 100) / (dataUser2.length),
        percentSimilarityTotal: (TotalSimilarities.length * 100) / (dataUser2.length),
        percentSimilarityEquals: (TotalSimilarities.length * 100) / (answersOnly1.length),
        questionsEqualsData: answersOnly1,
        equalAnswersData: TotalSimilarities
    };

}

async function getDataToSimilarityProcess(activityName, course, date1, date2, username1, username2, seccion) {

    if (!activityName) activityName = 'problem_graded';
    if (!course) throw "course is required";
    if (!date1) throw "date1 is required YY-MM-DD";
    if (!date2) throw "date2 is required YY-MM-DD";
    if (!username1) throw "username1 is required";
    if (!username2) throw "username2 is required";

    const data1 = await seguimientoModel.find({ name: activityName, course, date: date1, username: username1, seccion });
    const data2 = await seguimientoModel.find({ name: activityName, course, date: date2, username: username2, seccion });

    return [data1, data2]
}

async function getSimilarityAnswers(subAnswerUser1, answersUser2) {

    // console.log(subAnswerUser1, answersUser2);
    let similaritiesUser1 = [];

    for (let indexSubAnswer = 0; indexSubAnswer < subAnswerUser1.length; indexSubAnswer++) {
        const subSubAnswerUser1 = subAnswerUser1[indexSubAnswer];

        for (let indexAnswerUser2 = 0; indexAnswerUser2 < answersUser2.length; indexAnswerUser2++) {
            const answerUser2 = answersUser2[indexAnswerUser2].answers;

            // search answes 
            const searchAnswer = answerUser2.find(answer => answer === subSubAnswerUser1);
            if (searchAnswer) {
                console.log("array", answerUser2, "comparar", subSubAnswerUser1);
                similaritiesUser1.push({ subSubAnswerUser1, indexAnswer })
            }
        }

    }

    return similaritiesUser1;
}


async function createFormatDifferenceTime(_date1, _date2) {

    let difHour = '00';
    let difMin = '00';
    let difSec = '00';

    const date1 = new Date(_date1).getTime();
    const date2 = new Date(_date2).getTime();


    let diffTime = date1 > date2 ? date1 - date2 : date2 - date1;

    difSec = Math.floor(diffTime / 1000);

    if (difSec >= 60) {
        difHour = Math.floor(difSec / 3600);
        difHour = (difHour < 10) ? '0' + difHour : difHour;

        difMin = Math.floor((difSec / 60) % 60);
        difMin = (difMin < 10) ? '0' + difMin : difMin;

        difSec = difSec % 60;
        difSec = (difSec < 10) ? '0' + difSec : difSec;
    }

    return `${difHour}H:${difMin}M:${difSec}s`
}
module.exports = new SimilarityService();