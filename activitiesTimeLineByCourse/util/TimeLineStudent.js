const seguimiento = require('../../server/models/seguimiento');

class TimeLineDataCourse {

    // ==== number videos by day =====
    numVideosByCourseDate(username, course, date) {

        return new Promise((resolve, reject) => {
            seguimiento.find({
                $and: [
                    { username, course, date },
                    { $or: [{ name: "play_video" }, { name: "pause_video" }, { name: "stop_video" }] }
                ]
            }).countDocuments()
                .exec((err, numvideos) => {
                    if (err) reject("error on consult", err)
                    else resolve({ x: date, y: numvideos, group: 'Videos' })
                })
        })

    }

    // === number foros by day =====
    async numForosByCourseDate(username, course, date) {

        return new Promise((resolve, reject) => {
            seguimiento.find({
                $and: [
                    { username, course, date },
                    { $or: [{ name: "edx.forum.comment.created" }, { name: "edx.forum.response.created" }, { name: "edx.forum.thread.created" }] }
                ]
            }).countDocuments()
                .exec((err, numForos) => {
                    if (err) reject("error on consult", err)
                    else resolve({ x: date, y: numForos, group: 'Foros' })
                })
        })

    }

    async numContentByCourseDate(username, course, date) {

        return new Promise((resolve, reject) => {
            seguimiento.find({
                $and: [
                    { username, course, date },
                    { $or: [{ name: "nav_content" }, { name: "nav_content_click" }, { name: "nav_content_prev" }, { name: "nav_content_next" }, { name: "nav_content_tab" }] }
                ]
            }).countDocuments()
                .exec((err, numContent) => {
                    if (err) reject("error on consult", err)
                    else resolve({ x: date, y: numContent, group: 'Contenido' })
                })
        })

    }

    // ==== get all exament data =====

    async numExamenByCourseDate(username, course, date) {

        return new Promise((resolve, reject) => {
            seguimiento.find({
                $and: [
                    { username, course, date },
                    { name: "problem_check" }
                    // { $or: [{ name: "problem_graded" }, { name: "problem_check" }] }
                ]
            }).countDocuments()
                .exec((err, numContent) => {
                    console.log(numContent, "examenes", date, course);
                    if (err) reject("error on consult", err)
                    else resolve({ x: date, y: numContent, group: 'Examenes', label: numContent ? "Examen" : null })
                })
        })

    }

}

module.exports = TimeLineDataCourse