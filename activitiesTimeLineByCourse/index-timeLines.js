const express = require('express');
const bodyParser = require('body-parser');

const config = require('../config');

const activitiesTimeLines = require('./services/saveMetricsTimeLine');
const activitiesTimeLinesService = new activitiesTimeLines();

require('../server/database');
const cron = require('node-cron');

const app = express();
app.use(bodyParser.json());

app.listen(config.timesLimes.port, () => {
    console.log("time line by couse service on port", config.timesLimes.port);
});

cron.schedule('05 6 * * *', () => {
    activitiesTimeLinesService.saveAtivitiesTimeLineCourse();
});