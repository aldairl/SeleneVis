const TimeLineDataCourse = require('../util/TimeLineCourses');
const TimeLineStudentData = require('../util/TimeLineStudent');
const activitiesTimeLineModel = require('../../server/models/activitiesTimeLine.model');
const seguimiento = require('../../server/models/seguimiento');

class ActivitiesTimeLine {

    async saveAtivitiesTimeLineCourse() {

        const TimeLineDataCourseClass = new TimeLineDataCourse();
        const courses = await seguimiento.distinct("course");

        console.log("starting save data, number of courses", courses.length)

        if (courses.length) {

            let coursesSaved = 0;

            // for (const course of courses) {
            for (let indexCourse = 0; indexCourse < courses.length; indexCourse++) {
                const course = courses[indexCourse];

                // }

                const days = await seguimiento.find({ "course": course }).distinct('date')

                if (days.length) {
                    let generalData = [];
                    let count = 0;

                    // for (const day of days) {
                    for (let indexDay = 0; indexDay < days.length; indexDay++) {
                        const day = days[indexDay];

                        // }

                        Promise.all([
                            TimeLineDataCourseClass.numVideosByCourseDate(course, day),
                            TimeLineDataCourseClass.numContentByCourseDate(course, day),
                            TimeLineDataCourseClass.numForosByCourseDate(course, day),
                            TimeLineDataCourseClass.numExamenByCourseDate(course, day)
                        ]).then(async (result) => {

                            generalData.push(result[0], result[1], result[2], result[3]);
                            count++;

                            if (count === days.length) {

                                const conditions = { course }
                                    , update = { timelinedata: generalData }
                                    , options = { multi: true, upsert: true };
                                await activitiesTimeLineModel.updateOne(conditions, update, options);

                                coursesSaved += 1;
                                console.log('Fisnish', course, coursesSaved);
                                // resolve(generalData);
                                if (coursesSaved == courses.length) {
                                    console.log("saved all courses")
                                }
                            }
                        })
                    }

                }
            }
        } else {
            console.log("not exist courses");
        }
    }

    async getTimeLineStudent(student, course){

        const timeLineStudent = new TimeLineStudentData();

        console.log("get timeline of", student, course);

        // ==== get days ====
        const days = await seguimiento.find({ course, username }).distinct('date');

        if (days.length) {}
        else{
            console.log("not exist days");
        }

    }
}

module.exports = ActivitiesTimeLine;