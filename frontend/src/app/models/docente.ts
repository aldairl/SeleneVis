export class Docente {

       
    _id: string;
    name:string;
    course:string[];
    correo: string;
    password:string; 
    credencial: string;

   
    constructor(){
        this._id ='';
        this.name= '';
        this.course=[];
        this.correo='';
        this.password='';
        this.credencial='';

    }

    
}

// db.docentes.update({_id: ObjectId("5f54694b867b71c3e021ed8f")},{"password" : "$2y$10$KR6qNylSN9pcRNyHbar3/uV9YeZlkk6pd5Q19a.I1n.i5oOOcOK6.", "correo" : "admin@gmail.com", "credencial" : "Administrador", name:"admin", course:[]})