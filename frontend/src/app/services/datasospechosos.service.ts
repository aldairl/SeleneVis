import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DatasospechososService {

  readonly URL_API = environment.URL_BASE + 'sospechosos/';

  constructor(private http: HttpClient) { }

  getDataSospechososByCourse(course: string) {
    return this.http.post(this.URL_API, { course });
  }

  getPathImgExam(name: string) {
    return this.http.post(this.URL_API + 'img-exam', { nameImg: name });
  }

}
