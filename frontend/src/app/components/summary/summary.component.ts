import { Component, OnInit } from '@angular/core';
import { SeguimientoService } from '../../services/seguimiento.service';
import { IndicatorsService } from 'src/app/services/indicators.service';
import { Indicator } from 'src/app/models/indicators';
import { studentCourse } from 'src/app/models/studentCourse';
import { StadiscticGraph } from 'src/app/models/stadistics-graphs';
// import { Network, DataSet, Node, Edge, IdType, Graph2d } from 'vis';
import * as vis from 'vis';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoadingComponent } from '../shared/loading/loading.component';


@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})

export class SummaryComponent implements OnInit {

  indicators: Indicator[];
  stadistics: object;
  body: studentCourse = { course: '', student: '' };
  labelTable: string;
  course: string;
  loading = false;
  loadingIndicators = false;
  cleanByRound = true;

  existGraph = false;

  loadingComponentDialog: MatDialogRef<LoadingComponent>;

  constructor(private inicatorsCourseService: IndicatorsService, private staticsservice: SeguimientoService, public dialogLoading: MatDialog) {
  }

  ngOnInit() { }

  loadIndicatorsByCourse(course: string) {

    this.stadistics = null;
    if (course !== this.course || !this.indicators) {

      this.indicators = null;
      this.loadingIndicators = true;
      this.course = course;

      this.inicatorsCourseService.getIndicatorsByCourse(course).subscribe(indicators => {
        this.loadingIndicators = false;
        this.indicators = indicators;
      }, err => {
        this.loadingIndicators = false;
        console.log(err);
      })

      this.loadStadisticsByCourse();
    }
  }

  setBody(body: studentCourse) {

    this.openLoading();
    this.body = body;
    this.loadStadisticStudent();
    const student = this.body.student.split('_');
    this.labelTable = student[0] + ' ' + student[1]
  }

  loadStadisticStudent() {
    this.loading = true;
    this.staticsservice.getGeneralStaticsByUserAndCourse(this.body).subscribe(stadistics => {
      this.loading = false;
      this.stadistics = stadistics
      this.loadingComponentDialog.close();
    })
  }

  clear(event) {
    console.log("limpiar data", event);
    this.stadistics = null;
  }

  loadStadisticsByCourse() {

    this.openLoading();
    this.loadingIndicators = true;
    this.inicatorsCourseService.getStadisticsByCourse(this.course).subscribe((data: any) => {

      this.loadingComponentDialog.close();
      if (data && data.timelinedata) {
        console.log(data.timelinedata, "data of timeline");
        this.createStadisticsCourse(data.timelinedata);
      } else {
        console.log("error on data", data)
      }
    })
  }

  openLoading() {
    this.loadingComponentDialog = this.dialogLoading.open(LoadingComponent, {
      width: '350px',
      height: '350px'
    });
  }
  createStadisticsCourse(vectors: StadiscticGraph[]) {

    let graph2d;

    // let names = ["SquareShaded", "Bargraph", "Blank", "CircleShaded"];
    let groups = new vis.DataSet();

    // groups.add({
    //   id: 0,
    //   content: names[0],
    //   className: "custom-style1",
    //   options: {
    //     drawPoints: {
    //       style: "square", // square, circle
    //     },
    //     shaded: {
    //       orientation: "bottom", // top, bottom
    //     },
    //   },
    // });

    // groups.add({
    //   id: 1,
    //   content: names[1],
    //   className: "custom-style2",
    //   options: {
    //     style: "bar",
    //     drawPoints: { style: "circle", size: 10 },
    //   },
    // });

    // groups.add({
    //   id: 2,
    //   content: names[2],
    //   options: {
    //     yAxisOrientation: "right", // right, left
    //     drawPoints: false,
    //   },
    // });

    // groups.add({
    //   id: 3,
    //   content: names[3],
    //   className: "custom-style3",
    //   options: {
    //     yAxisOrientation: "right", // right, left
    //     drawPoints: {
    //       style: "circle", // square, circle
    //     },
    //     shaded: {
    //       orientation: "top", // top, bottom
    //     },
    //   },
    // });

    let container = document.getElementById("visualization");
    container.innerHTML = "";

    let items = vectors;

    let dataset = new vis.DataSet(items);

    let options = {
      drawPoints: {
        enabled: true,
        onRender: function (item, group, graph2d) {
          // only renders items with labels
          if (item.label != null) {
            return {
              style: 'circle',
              size: 10
            };
          }
          //return item.label != null;
        }
      },
      defaultGroup: "",
      dataAxis: {
        showMinorLabels: true,
        right: {
          title: {
            text: "actividades",
          },
        },
      },
      legend: { left: { position: "top-left" } },
      start: vectors[0].x,
      end: vectors[(vectors.length - 1)].x
    };


    graph2d = new vis.Graph2d(container, dataset, groups, options);


    graph2d.on('rangechange', function (props) {
      graph2d.redraw()
    });

  }

}
