import { studentCourse } from 'src/app/models/studentCourse';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Grafo } from 'src/app/models/grafos';
import { StadisticByControl } from 'src/app/models/stadisticByControl';
import { DatasospechososService } from 'src/app/services/datasospechosos.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoadingComponent } from '../shared/loading/loading.component';

@Component({
  selector: 'app-grafoscomparation',
  templateUrl: './grafoscomparation.component.html',
  styleUrls: ['./grafoscomparation.component.css']
})
export class GrafoscomparationComponent implements OnInit {

  body: studentCourse = { course: null, student: null };
  bodysuspicious: studentCourse = { course: null, student: null };
  grafoService: Grafo = { edges: [], nodes: [], options: [] };
  optionSelected = '';

  stadistics: object;

  courseSelected = null;
  GeneralData = null;
  listStudents = null;
  listSuspicious = null;

  loadingComponentDialog: MatDialogRef<LoadingComponent>;

  constructor(private sospechososService: DatasospechososService, public dialogLoading: MatDialog) { }

  ngOnInit(): void {
  }

  // set course to get students
  getStudentsByCourseAndSuspicious(course: string) {

    this.openLoading();
    this.clearView();
    this.body.course = course;
    this.bodysuspicious.course = course;

    this.courseSelected = course;
    this.sospechososService.getDataSospechososByCourse(course).subscribe(data => {

      this.listStudents = []
      // console.log(data, "students");
      if (data[0]) {
        this.GeneralData = data[0];

        // this.listStudents = data[0].examenes[0].estudiantes.map((student: any) => student.username);
        // this.listSuspicious = data[0].examenes[0].sospechosos.map((suspicious: any) => suspicious.username);

        for (let index = 0; index < this.GeneralData.examenes.length; index++) {
          const objExam = this.GeneralData.examenes[index];
          this.listStudents = this.listStudents.concat(objExam.estudiantes);
        }

        this.listStudents = this.listStudents.map((student: any) => student.username);
        this.listStudents = [...new Set(this.listStudents)];

      }
      this.loadingComponentDialog.close();

    }, error => {
      console.log(error);
      this.loadingComponentDialog.close();
    })
  }

  selectStudentToBody(student: string) {

    if (this.body.student) {

      this.body.student = null;
      setTimeout(() => { this.body.student = student }, 50);
    } else {

      this.body.student = student;
    }

  }

  setStudentSuspicious(student: string) {
    if (this.bodysuspicious.student) {

      this.bodysuspicious.student = null;
      setTimeout(() => { this.bodysuspicious.student = student }, 50);

    } else {
      this.bodysuspicious.student = student;
    }
  }

  createStadistics(stadisticsByControl: StadisticByControl) {
    this.stadistics = stadisticsByControl.data;
  }

  clearView() {
    this.body = { course: null, student: null };
    this.bodysuspicious = { course: null, student: null };
  }

  openLoading() {
    this.loadingComponentDialog = this.dialogLoading.open(LoadingComponent, {
      width: '350px',
      height: '350px'
    });
  }

}
