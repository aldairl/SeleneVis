import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrafoscomparationComponent } from './grafoscomparation.component';

describe('GrafoscomparationComponent', () => {
  let component: GrafoscomparationComponent;
  let fixture: ComponentFixture<GrafoscomparationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrafoscomparationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrafoscomparationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
