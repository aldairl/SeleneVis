import { Component, OnInit, Input, ViewChild, OnChanges, AfterViewInit } from '@angular/core';
import { Indicator } from 'src/app/models/indicators';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-table-stadistics',
  templateUrl: './table-stadistics.component.html',
  styleUrls: ['./table-stadistics.component.css']
})
export class TableStadisticsComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() indicators: Indicator[];
  @Input() course: string;

  // indicators = [ {name:"carlos", age:"12", cc:"105"} ];
  // "T. Examen", "T. Videos", "T. Otros" , "#SesionesDif", "#VideosDif",
  columnsToDisplay = ['student', 'Videos', 'Contenido', 'Foros', "Examenes", 'Sesiones'];

  dataSource: MatTableDataSource<Indicator>

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  // MatPaginator Inputs
  pageSizeIndicators = 100
  // this.indicators ? this.indicators.length : 0;
  length = this.pageSizeIndicators;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, this.pageSizeIndicators];

  stadistics: object;
  loading = false;
  resultsLength = 0;

  constructor() { }

  ngOnInit(): void {
    this.buildGraps();
    console.log(this.indicators, "indicatots");
    
  }

  ngAfterViewInit() {
    this.dataSource = new MatTableDataSource<Indicator>(this.indicators);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnChanges() {
  }

  orderAlphabeticalNames() {
    if (this.course && this.indicators) {
      // order alphabetilcally
      this.indicators.sort((a, b) => {
        if (a.student > b.student)
          return 1;
        if (a.student < b.student)
          return -1;
        return 0;
      })
    }
  }

  clear() {
    this.stadistics = null;
  }

  buildGraps() {

    this.loading = true;
    this.dataSource = new MatTableDataSource<Indicator>(this.indicators);
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;

    this.getStadisticsByIndicators().then((stadiscticsbycourse: object) => {

      this.loading = false;
      this.stadistics = stadiscticsbycourse;
      this.indicators = [];

    }).catch(err => {
      this.loading = false;
      console.log(err)
    })
  }

  getStadisticsByIndicators() {

    return new Promise((resolve, reject) => {

      const numdocs = this.indicators.length;
      let count = 0;

      if (numdocs) {

        let stadiscticsbycourse = {
          Videos: 0,
          Contenido: 0,
          Foros: 0,
          Examenes: 0,
          Sesiones: 0,
          // VideosDiferentes: 0,
          // SesionesDiferentes: 0,
          TimeVideos: 0,
          TimeExam: 0,
          TimeOthers: 0
        }

        // get stadistics of course
        for (const indicator of this.indicators) {
          count++;
          stadiscticsbycourse.Videos += parseInt(indicator.Videos);
          stadiscticsbycourse.Contenido += parseInt(indicator.Contenido);
          stadiscticsbycourse.Foros += parseInt(indicator.Foros);
          stadiscticsbycourse.Examenes += parseInt(indicator.Examenes);
          stadiscticsbycourse.Sesiones += parseInt(indicator.Sesiones);
          // stadiscticsbycourse.VideosDiferentes += parseInt(indicator.numVideosDiferentes);
          // stadiscticsbycourse.SesionesDiferentes += parseInt(indicator.numSesionesDif);
          stadiscticsbycourse.TimeVideos += parseInt(indicator.TimeVideos);
          stadiscticsbycourse.TimeExam += parseInt(indicator.TimeExam);
          stadiscticsbycourse.TimeOthers += parseInt(indicator.TimeOthers);
        }
        if (count == numdocs) {
          resolve(stadiscticsbycourse);
        }
      } else {
        reject({})
      }

    })
  }

}
