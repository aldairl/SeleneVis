import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DatasospechososService } from 'src/app/services/datasospechosos.service';
import { environment } from 'src/environments/environment';
import { LoadingComponent } from '../shared/loading/loading.component';

@Component({
  selector: 'app-comparison',
  templateUrl: './comparison.component.html',
  styleUrls: ['./comparison.component.css']
})
export class ComparisonComponent implements OnInit {

  courses = [];
  courseSelected: string;
  examSelected: string;

  studentSelected = { username: null };
  studentSuspiciousSelected = { username: null };

  listStudents = [];
  listSuspicious = [];
  exams = [];
  urlimage = environment.PATH_IMAGE;

  imgSuspicious = null;
  imgSuspicious_T = null;
  imgTotal = null;
  imgTotal_T = null;

  GeneralData = null;
  matrizComparation = null;

  displayedColumns: string[] = ['Coincidencias', 'Dif. tiempo', 'Número preguntas', 'porcentaje'];
  dataSource = null;

  loading = false;
  loadingComponentDialog: MatDialogRef<LoadingComponent>;

  constructor(private sospechososService: DatasospechososService, public dialogLoading: MatDialog) {
  }

  ngOnInit(): void {
  }

  setCourse(course) {
    this.courseSelected = course;
  }

  setStudents(student: string) {
    this.dataSource = null;
    this.studentSelected = { username: null };

    const dataOfExamn = this.GeneralData.examenes.find((infoExamn: any) => infoExamn.name_exam === this.examSelected);
    // this.studentSelected = dataOfExamn.estudiantes.find((normalstudent: any) => normalstudent.username == student);
    this.studentSelected = dataOfExamn.sospechosos.find((normalstudent: any) => normalstudent.username == student);
  }

  setStudentSuspicious(student: string) {
    this.dataSource = null;
    const dataOfExamn = this.GeneralData.examenes.find((infoExamn: any) => infoExamn.name_exam === this.examSelected);
    this.studentSuspiciousSelected = dataOfExamn.sospechosos.find((suspicious: any) => suspicious.username == student);
  }

  setExam(exam: string) {
    this.examSelected = exam;
    this.createPathImage(this.examSelected).then((url: string) => this.imgSuspicious = url);
    this.createPathImage(this.examSelected + '_T').then((url: string) => this.imgSuspicious_T = url);
    // this.listStudents = [];
    // this.listSuspicious = [];
    this.studentSelected = { username: null };
    this.studentSuspiciousSelected = { username: null };
    this.loadDataStudents();
  }

  loadstudentsExamByCourse(course: string) {

    this.openLoading();
    this.clearData();
    this.courseSelected = course;
    this.sospechososService.getDataSospechososByCourse(course).subscribe(data => {

      if (data[0]) {
        this.GeneralData = data[0];
        this.exams = data[0].examenes.map((infoExamn: any) => infoExamn.name_exam);
        // this.listStudents = data[0].examenes[0].estudiantes.map((student: any) => student.username);
        this.listStudents = data[0].examenes[0].sospechosos.map((student: any) => student.username);
        this.listSuspicious = data[0].examenes[0].sospechosos.map((suspicious: any) => suspicious.username);
        this.matrizComparation = data[0].examenes[0].matriz;
      }
      this.loadingComponentDialog.close();
    }, error => {
      this.loadingComponentDialog.close();
      console.log(error);

    })

    this.createPathImage('Total').then(url => this.imgTotal = url);
    this.createPathImage('Total_T').then(url => this.imgTotal_T = url);
  }


  loadDataStudents() {
    const dataOfExamn = this.GeneralData.examenes.find((infoExamn: any) => infoExamn.name_exam === this.examSelected);
    // this.listStudents = dataOfExamn.estudiantes.map((student: any) => student.username);
    this.listStudents = dataOfExamn.sospechosos.map((student: any) => student.username);
    this.listSuspicious = dataOfExamn.sospechosos.map((suspicious: any) => suspicious.username);
    this.matrizComparation = dataOfExamn.matriz;
  }

  createPairComparation() {

    const pairStudents = this.studentSelected.username + '-' + this.studentSuspiciousSelected.username;

    const dataComparation = this.matrizComparation.filter((objmatriz: any) => objmatriz.pareja == pairStudents);
    this.dataSource = dataComparation;

    // crete difference of time
    // this.dataSource[0].dif_tiempo = Math.round(Number(differenceSeconds)) ? `${differenceminutes} : ${Math.round(Number(differenceSeconds) * 60)}` : '0';
    this.dataSource[0].dif_tiempo = this.createFormatDifferenceTime();
    // round percent
    this.dataSource[0].porcentaje = Math.round(Number(this.dataSource[0].porcentaje));
    // coincidencias
    this.dataSource[0].coincidencias = Math.round(Number(this.dataSource[0].coincidencias));

    // date and hour
    this.dataSource[0].date1 = new Date(this.dataSource[0].date1).toLocaleString('en-US');
    this.dataSource[0].date2 = new Date(this.dataSource[0].date2).toLocaleString('en-US');
  }

  createPathImage(nameImg: string) {

    return new Promise((resolve, reject) => {

      let imgSuspicious = `${this.courseSelected}/${nameImg}.png`;

      this.sospechososService.getPathImgExam(imgSuspicious).subscribe((urlImage: string) => {

        resolve(urlImage);

      }, (error) => {

        console.log("error", error)
        reject(error);
      })
    })
  }

  getImgExamen() {

    this.createPairComparation();

  }

  openLoading() {
    this.loadingComponentDialog = this.dialogLoading.open(LoadingComponent, {
      width: '350px',
      height: '350px'
    });
  }

  createFormatDifferenceTime() {

    let difHour: any = '00';
    let difMin: any = '00';
    let difSec: any = '00';

    const date1 = new Date(this.dataSource[0].date1).getTime();
    const date2 = new Date(this.dataSource[0].date2).getTime();


    let diffTime = date1 > date2 ? date1 - date2 : date2 - date1;

    difSec = Math.floor(diffTime / 1000);

    if (difSec >= 60) {
      difHour = Math.floor(difSec / 3600);
      difHour = (difHour < 10) ? '0' + difHour : difHour;

      difMin = Math.floor((difSec / 60) % 60);
      difMin = (difMin < 10) ? '0' + difMin : difMin;

      difSec = difSec % 60;
      difSec = (difSec < 10) ? '0' + difSec : difSec;
    }

    return `${difHour}H:${difMin}M:${difSec}s`
  }

  clearData() {

    this.courseSelected = null;
    this.examSelected = null;
    this.studentSelected = { username: null };
    this.studentSuspiciousSelected = { username: null };
    this.listStudents = [];
    this.listSuspicious = [];
    this.exams = [];

    this.imgSuspicious = null;
    this.imgSuspicious_T = null;
    this.imgTotal = null;
    this.imgTotal_T = null;

  }
}
