import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatasospechososService } from 'src/app/services/datasospechosos.service';

@Component({
  selector: 'app-examenlist',
  templateUrl: './examenlist.component.html',
  styleUrls: ['./examenlist.component.css']
})
export class ExamenlistComponent implements OnInit {

  @Input() course: string;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() listData = [];
  @Output() sendOption = new EventEmitter<string>();
  // @Output() sendSospechosos = new EventEmitter<[object]>();

  examSelected: string;

  constructor() { }

  ngOnInit(): void {
  }

  sendOptionSelected(option:string){
    this.sendOption.emit(option);
  }

}