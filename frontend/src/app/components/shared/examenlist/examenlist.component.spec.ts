import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamenlistComponent } from './examenlist.component';

describe('ExamenlistComponent', () => {
  let component: ExamenlistComponent;
  let fixture: ComponentFixture<ExamenlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamenlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamenlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
