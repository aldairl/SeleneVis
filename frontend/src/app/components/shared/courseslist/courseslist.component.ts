import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DocenteService } from 'src/app/services/docente.service';
import { Docente } from 'src/app/models/docente';

@Component({
  selector: 'app-courseslist',
  templateUrl: './courseslist.component.html',
  styleUrls: ['./courseslist.component.css']
})
export class CourseslistComponent implements OnInit {

  @Output() sendCourse = new EventEmitter<string>();
  courses = [];
  courseSelected: string;

  constructor(private docenteservice: DocenteService) {
    this.loadCourses();
  }

  ngOnInit(): void {
  }

  loadCourses() {
    const user = JSON.parse(localStorage.getItem('usuario'));
    // console.log( user.course);
    this.courses = user.course;
    this.docenteservice.getDocente(user._id).subscribe((docente: Docente) => {
      this.courses = docente.course;
    })

  }

  loadstudentsByCourse(course) {
    this.sendCourse.emit(course);
  }

}
