import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Grafo } from 'src/app/models/grafos';
import { StadisticByControl } from 'src/app/models/stadisticByControl';
import { studentCourse } from 'src/app/models/studentCourse';
import { GrafosService } from 'src/app/services/grafos.service';
import { IndicatorsService } from 'src/app/services/indicators.service';
import { Network, DataSet, Node, Edge } from 'vis';
import { LoadingComponent } from '../loading/loading.component';

@Component({
  selector: 'app-grafostadisctics',
  templateUrl: './grafostadisctics.component.html',
  styleUrls: ['./grafostadisctics.component.css']
})
export class GrafostadiscticsComponent implements OnInit, OnDestroy {

  @Input() body: studentCourse = { course: null, student: null };
  @Input() idNetwork: string = 'mynetwork';

  selectedValue = 'dia';
  optionSelected = '';
  grafoService: Grafo = { edges: [], nodes: [], options: [] };
  stadisticsGeneral: StadisticByControl[];
  stadistics: object;

  public graphNetwork: Network;
  cleanByRound = true;
  labelTable: string;

  loading = false;
  loadingComponentDialog: MatDialogRef<LoadingComponent>;

  constructor(private grafosService: GrafosService, private servicesStadistics: IndicatorsService, public dialogLoading: MatDialog) { }

  ngOnInit(): void {
    if (this.body.student) {
      this.getGrafos();
    }
  }

  ngOnDestroy() {
    this.deleteAllGraphs();
  }

  // === get grafos general ===
  getGrafos() {

    this.openLoading();
    // this.clearallGraphics();
    // this.body = body;
    const student = this.body.student.split('_');
    this.labelTable = student[0] + ' ' + student[1]

    if (this.selectedValue == "session") {
      this.getGrafosBySession(this.body);
    } else {
      this.getGrafosByDay(this.body);

    }
  }

  openLoading() {
    this.loadingComponentDialog = this.dialogLoading.open(LoadingComponent, {
      width: '350px',
      height: '350px'
    });
  }

  // === when select another option ===
  // opton can be days or sessions
  getGrafoByOption(option) {

    this.openLoading();

    const body = { ...this.body, control: option };

    if (this.selectedValue == 'dia') {

      this.servicesStadistics.getStadisticByDay(body).subscribe(data => {
        console.log(data, "data de stadisctic")
        this.stadisticsGeneral = data;
        this.createStadistics(data[0]);
        this.loadingComponentDialog.close();

      }, error => {
        console.log(error);
        this.loadingComponentDialog.close();
      })

    } else {

      this.servicesStadistics.getStadisticBySession(body).subscribe(data => {
        console.log(data, "data de stadisctic")

        this.stadisticsGeneral = data;
        this.createStadistics(data[0]);
        this.loadingComponentDialog.close();
      })
    }

    this.createNetworkGeneral(this.grafoService, this.idNetwork, option);

  }

  // === By session ===
  getGrafosBySession(body: studentCourse) {
    this.selectedValue = 'session';

    this.servicesStadistics.getStadisticBySession(body).subscribe(data => {
      this.stadisticsGeneral = data;
      this.createStadistics(data.find(obj => obj.control === this.optionSelected));
      this.loadingComponentDialog.close();

    });

    this.grafosService.getGrafosBySession(body).subscribe((grafo: Grafo) => {

      this.grafoService = grafo;
      this.createNetworkGeneral(grafo, this.idNetwork);
      this.loadingComponentDialog.close();

    })


  }

  // === By Day ===
  getGrafosByDay(body: studentCourse) {
    // console.log(stadisticsByControl);
    this.selectedValue = 'dia';

    this.servicesStadistics.getStadisticByDay(body).subscribe(data => {

      this.stadisticsGeneral = data;
      this.optionSelected = data[0].control
      // console.log(this.stadisticsGeneral)
      this.createStadistics(data.find(obj => obj.control === this.optionSelected));
      this.loadingComponentDialog.close();
    })

    this.grafosService.getGrafosByDay(body).subscribe((grafo: Grafo) => {

      this.grafoService = grafo;
      this.createNetworkGeneral(grafo, this.idNetwork);
      this.loadingComponentDialog.close();

    })

  }

  createNetworkGeneral(grafo: Grafo, nameContainer: string, dayOrSessionOption?: string) {

    this.optionSelected = grafo.options[0]['control'] || grafo.options[0];

    // this.optionSelected = this.grafoService.options[0];

    // const nodes = new DataSet(this.grafoService.nodes)
    const nodes = new DataSet(grafo.nodes);
    // const nodes = new DataSet([
    //   { id: 1, label: 'Node 1' },
    // ]);

    // const edges2 = new DataSet(nodesOnOptions.nodesOrder);
    let edges2 = new DataSet(grafo.edges[0].nodesOrder);
    // create an array with edges
    // const edges = new DataSet([
    //   { from: 1, to: 3 },
    // ]);

    if (dayOrSessionOption) {
      const nodesOnOptions = grafo.edges.find(objedge => objedge.day === dayOrSessionOption);
      edges2 = new DataSet(nodesOnOptions.nodesOrder);
    }

    // create a network
    // const container2 = document.getElementById('mynetwork');
    const container = document.getElementById(nameContainer);

    const data2 = {
      nodes: nodes,
      edges: edges2
    };

    const options = {
      nodes: {
        shape: "circle",
        font: {
          size: 20,
          color: "#6e6e6e",
          strokeColor: '#ffffff',
          strokeWidth: 4
        },
        size: 30
      },
      interaction: { hover: true },
      edges: {
        arrows: 'to',
        font: {
          color: '#000',
          size: 15,
          strokeColor: '#ffffff',
          align: 'horizontal'
        },
        shadow: {
          enabled: true,
          color: 'rgba(0,0,0,0.5)',
          size: 10,
          x: 5,
          y: 5
        },
      },
      physics: {
        forceAtlas2Based: {
          gravitationalConstant: -26,
          centralGravity: 0.005,
          springLength: 230,
          springConstant: 0.18
        },
        maxVelocity: 146,
        solver: "forceAtlas2Based",
        timestep: 0.35,
        stabilization: { iterations: 5 }
      }
    };


    this.graphNetwork = new Network(container, data2, options);

  }

  createStadistics(stadisticsByControl: StadisticByControl) {
    this.stadistics = stadisticsByControl.data;
    this.loadingComponentDialog.close();
  }

  clearStadistic() {
    this.stadistics = null;
  }

  deleteAllGraphs() {
    this.graphNetwork.destroy();
  }
}
