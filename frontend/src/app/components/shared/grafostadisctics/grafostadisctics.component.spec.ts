import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrafostadiscticsComponent } from './grafostadisctics.component';

describe('GrafostadiscticsComponent', () => {
  let component: GrafostadiscticsComponent;
  let fixture: ComponentFixture<GrafostadiscticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrafostadiscticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrafostadiscticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
