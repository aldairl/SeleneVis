import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DocentesComponent } from './components/docentes/docentes.component';
import { SummaryComponent } from './components/summary/summary.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NavbarComponent } from './components/shared/navbar/navbar.component'; 
import { MaterialModule } from './modules/material.module';
import { ChartsModule } from 'ng2-charts';
import { BarsComponent } from './components/shared/bars/bars.component';
import { TableStadisticsComponent } from './components/table-stadistics/table-stadistics.component';
import { ListCoursesStudentsComponent } from './components/shared/list-courses-students/list-courses-students.component';
import { LoginComponent } from './components/shared/login/login.component';
import { ComparisonComponent } from './components/comparison/comparison.component';
import { CourseslistComponent } from './components/shared/courseslist/courseslist.component';
import { ExamenlistComponent } from './components/shared/examenlist/examenlist.component';
import { GrafostadiscticsComponent } from './components/shared/grafostadisctics/grafostadisctics.component';
import { GrafoscomparationComponent } from './components/grafoscomparation/grafoscomparation.component';
import { LoadingComponent } from './components/shared/loading/loading.component';

@NgModule({
  declarations: [
    AppComponent,
    DocentesComponent,
    SummaryComponent,
    NavbarComponent,
    BarsComponent,
    TableStadisticsComponent,
    ListCoursesStudentsComponent,
    LoginComponent,
    ComparisonComponent,
    CourseslistComponent,
    ExamenlistComponent,
    GrafostadiscticsComponent,
    GrafoscomparationComponent,
    LoadingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
