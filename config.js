module.exports = {
    timesLimes: {
        port: process.env.TIME_LINES_PORT || 8001
    },
    graphsDay: {
        port: process.env.GRAPHS_DAY_PORT || 8002
    },
    graphsSession: {
        port: process.env.GRAPHS_SESSION_PORT || 8003
    },
    stadisticStudent: {
        port: process.env.STADISTIC_STUDENT_PORT || 8004
    },
    stadisticTotalStudent: {
        port: process.env.STADISTIC_STUDENT_PORT || 8005
    },
    similarities: {
        port: process.env.SIMILARITIES_PORT || 8006
    }
}