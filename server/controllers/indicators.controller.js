const Indicator = require('../models/indicator')

const indicatorCtrl = {}; //he definido un objeto para luego aplicar metodos.

indicatorCtrl.getIndicators = async (req, res) => {

    const indicator = await Indicator.find();
    res.json(indicator);
};

indicatorCtrl.createIndicator = async (req, res) => {

    if (req.body) {
        indicator = new Indicator(req);

        await indicator.save();
        // res.json('saved');
        return 'indicator saved sussesfuly';
    }
    else {

        const conditions = { _id: req._id }
            , update = req
            , options = { multi: true, upsert: true };

        await Indicator.updateOne(conditions, update, options);
        // await Indicator.save(req)
        return `'indicator saved sussesfuly' ${Indicator}`;
    }

}

indicatorCtrl.getIndicatorsByCourse = async (req, res) => {

    const indicatorsBycourse = await Indicator.find({ course: req.body.course });

    res.json(indicatorsBycourse);
}

indicatorCtrl.getIndicatorsByStudent = async (req, res) => {
    const indicatorsBycourse = await Indicator.find({ student: req.body.student, course: req.body.course });
    const { Videos, Contenido, Foros, Examenes, Sesiones,
        numSesionesDif, numVideosDiferentes, TimeVideos, TimeExam, TimeOthers } = indicatorsBycourse[0];

    res.json({
        Videos, Contenido, Foros, Examenes, Sesiones,
        numSesionesDif, numVideosDiferentes, TimeVideos, TimeExam, TimeOthers
    });
}

module.exports = indicatorCtrl;
