const sospechosos = require('../models/sospechosos')
const comparations = {};
const fs = require('fs');
const path = require('path');

comparations.getDataExam = async (req, res) => {
    const course = req.body.course;
    getdataByExam(course).then(data => res.json(data))

}

comparations.getImgExam = async (req, res) => {
    const nameImg = req.body.nameImg;
    getPathImg(nameImg)
        .then(url => res.json(url))
        .catch(err => res.json(err))
}

function getdataByExam(course) {

    return new Promise((resolve, reject) => {

        sospechosos.find({ course: course })
            .exec((err, data) => {
                if (err) {
                    reject(`error en la consulta ${err}`);
                } else {
                    resolve(data);
                }
            })
    });
}

function getPathImg(nameImg) {

    // the parameter nameImg contain the name of course and name img

    return new Promise((resolve, reject) => {

        try {

            // split the path nameImg to get only name
            // const nameTosave = nameImg.split('/')[1];
            // const nameTosave = "Screenshot.png";

            // read the file on the ubication
            // const img = fs.readFileSync(`/home/think/Downloads/${nameTosave}`);
            // const img = fs.readFileSync(`/home/vagrant/seguimiento2020/imagenes/${nameImg}`);

            // move file to path publlic on server
            // fs.writeFileSync(path.join(__dirname, `../assetsPublic/images/${nameTosave}`), img);

            // build the URL image
            // const url = `http://localhost:8000/files/images/${nameImg}`;
            // const url = `/files/images/${nameTosave}`;
            const url = `/files/images/${nameImg}`;

            resolve(url);

        } catch (error) {
            reject(error);
        }
    })
}

module.exports = comparations;