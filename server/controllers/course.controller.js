const Course = require('../models/course');
const courseService = require('../services/courses.service');

class CourseController {

    async getCourses(req, res) {
        const cursos = await Course.find();
        res.json(cursos);
    }

    async createCourse(req, res) {
        if (req.body) {
            course = new Course(
                {
                    name: req.body.name,
                    students: req.body.students
                });

            await course.save();
            res.json('saved');
            return 'course saved sussesfuly';

        }
        else {
            const course = new Course(
                {
                    name: req.name,
                    students: req.students
                });
            await course.save();
            respuesta = 'course saved sussesfuly';
            return respuesta;

        }
    }

    async getCourse(req, res) {
        console.log(req.params.id)
        const docent = await Course.findById(req.params.id);
        res.json(docent);
    }

    async editCourse(req, res) {
        const { id } = req.params;
        const course = {
            name: req.body.name,
            curso: req.body.students,
        }
        await Course.findByIdAndUpdate(id, { $set: course }, { new: true });
        res.json({
            "status": 'course update'
        });
    }

    async deleteCourse(req, res) {
        await Course.findByIdAndRemove(req.params.id);
        res.json({
            status: 'Course Deleted'
        });
    }

    async createDataCourse(req, res) {

        courseService.execTaskData();

        res.json({
            success: true,
            data: "data is saving"
        })
    }
}


module.exports = new CourseController();
