const Docente = require('../models/docente');
var bcrypt = require('bcryptjs');

const mdAuth = require('../middlewares/tokenverification');

const docenteCtrl = {}; //he definido un objeto para luego aplicar metodos.
docenteCtrl.getDocentes = async (req, res) => {
    const docentes = await Docente.find();
    res.json(docentes);
};

docenteCtrl.createDocente = async (req, res) => {
    const docent = new Docente(
        {
            name: req.body.name,
            course: req.body.course,
            correo: req.body.correo,
            password: bcrypt.hashSync(req.body.password, 10),
            credencial: req.body.credencial
        });

    docent.save().then(res => res.json({ status: 'Docente guardado' }))
        .catch(err => res.json({ status: err }))

}


docenteCtrl.getDocente = async (req, res) => {
    const docent = await Docente.findById(req.params.id);
    res.json(docent);
}

docenteCtrl.getDocenteByName = async (req, res) => {

    const body = req.body;
    const docent = await Docente.findById(body.email);
    res.json(docent);
}

docenteCtrl.editDocente = async (req, res) => {
    const { id } = req.params;
    const docent = {
        name: req.body.name,
        course: req.body.course,
        correo: req.body.correo,
        password: bcrypt.hashSync(req.body.password, 10),
        credencial: req.body.credencial
    }
    await Docente.findByIdAndUpdate({_id: id}, { $set: docent }, {useFindAndModify: false})
        .then(response => {
            res.json({
                "status": 'docente update',
                response,
                error: null
            })
        })
        .catch(err => {
            res.json({
                "status": 'docente update error',
                error: err
            })
        }
        )
}

docenteCtrl.deleteDocente = async (req, res) => {
    await Docente.findByIdAndRemove(req.params.id);
    res.json({
        status: 'docente Deleted'
    });
};



module.exports = docenteCtrl;
