const { PythonShell } = require('python-shell');

class ExecPythonImages {

    copyImages() {
        let options = {
            mode: 'text',
            pythonOptions: ['-u'],
            scriptPath: `${__dirname}`,
            args: [] //An argument which can be accessed in the script using sys.argv[1]
        };

        PythonShell.run('copyImages.py', options, function (err, result) {
            if (err)
                throw err;
            console.log('result: ', result);
        });
    }

}

module.exports = ExecPythonImages;
