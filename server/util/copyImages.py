import shutil
import os

try:
    
    src = '/home/vagrant/seguimiento2020/imagenes/';
    # src = '/home/think/Downloads/';
    dst = '../server/assetsPublic/images';

    #verify is exist folder
    if os.path.exists(dst):
        shutil.rmtree(dst) #remove the folder
    
    shutil.copytree(src, dst); #create and copy folder

except OSError as e:
    print(e)