const mongoose = require('mongoose');

// const URI= 'mongodb://localhost/selene';
//const URI = 'mongodb://127.0.0.1:27017/seguimiento';

const {
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_HOSTNAME,
  MONGO_PORT,
  MONGO_DB
} = process.env;

const options = {
  useNewUrlParser: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 500,
  connectTimeoutMS: 10000,
};

const url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`;

mongoose.connect(url, options)
  .then(() => console.log('MongoDB is connected'))
  .catch(err => console.error("Error conecting db ", err));


//module.exports = mongoose;