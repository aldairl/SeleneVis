const mongoose = require('mongoose');
const { Schema } = mongoose;

const stadistic = new Schema({

    _id: String,
    student: String,
    course: String,
    data: Object,
    control: String,
    controlDS: String,
    days: [String],
    sessions: [String]

});

module.exports = mongoose.model('stadistic', stadistic);