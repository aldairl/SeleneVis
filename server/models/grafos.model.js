const mongoose = require('mongoose');
const { Schema } = mongoose;

const GrafoSchema = new Schema({
    _id: String,
    student: String,
    course: String,
    edges: [Object],
    nodes: [Object],
    options: [Object],
    control: String
});

module.exports = mongoose.model('grafo', GrafoSchema);