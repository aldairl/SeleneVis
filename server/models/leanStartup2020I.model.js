const mongoose = require('mongoose');
const { Schema } = mongoose;

const leanStartup2002I = new Schema({
    username: String,
    answers: String,
    course: String,
    session: String,
    date: String,
    unit: String,
    date_time: String,
    name: String,
    section: String,
    subsection: String,
    time: String,
    page: String,
    seccion: String,
    sub_seccion: String
});

module.exports = mongoose.model('leanstartup2020I', leanStartup2002I);