const mongoose = require('mongoose');
const { Schema } = mongoose;
const activitiesTimeLineSchema = new Schema({
    course: String,
    timelinedata: [Object]
});
module.exports = mongoose.model('activitiesTimeLine', activitiesTimeLineSchema);
// {
    // x: String,
    // y: String,
    // group: String,
    // course: String
// }