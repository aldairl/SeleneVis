const mongoose = require('mongoose');
const course = require('./course');
const { Schema } = mongoose;

const indicatorSchema = new Schema({

    // idCourseStudent:String,
    _id: String,
    student: String,
    course: String,
    Videos: String,
    Contenido: String,
    Foros: String,
    Examenes: String,
    Sesiones: String,
    numSesionesDif: String,
    numVideosDiferentes: String,
    TimeVideos: String,
    TimeExam: String,
    TimeOthers: String,

    // timeForos:String,
    // numOtros:String
    // timeContenido: {type:String,required: true/*,unique:true*/},
    // timeSesiones: {type:String,required: true/*,unique:true*/},
    // timeTotal: String,
    // numRespuestas: {type:String,required: true/*,unique:true*/},
});
//CourseSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Indicator', indicatorSchema);