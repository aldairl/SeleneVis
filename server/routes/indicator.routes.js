const express = require('express');
const router1 = express.Router();
const indicator = require('../controllers/indicators.controller');

router1.get('/', indicator.getIndicators);
router1.post('/', indicator.createIndicator);
router1.post('/course', indicator.getIndicatorsByCourse);
router1.post('/student', indicator.getIndicatorsByStudent);

module.exports = router1;