const express = require('express');
const router =express.Router();
const compatationController = require('../controllers/comparations.controller');

router.post('/', compatationController.getDataExam );
router.post('/img-exam', compatationController.getImgExam );

module.exports = router;