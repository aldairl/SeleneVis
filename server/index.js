const express = require('express');
const morgan = require('morgan');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
const { mongoose } = require('./database');

app.set('port', process.env.PORT || 8080);

//Middlewares

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

// Routes
app.use('/api/docentes', require('./routes/docente.routes'));
app.use('/api/seguimiento', require('./routes/seguimiento.routes'));
app.use('/api/courses', require('./routes/course.routes'));
app.use('/api/studentts', require('./routes/studentt.routes'));
app.use('/api/indicators', require('./routes/indicator.routes'));
app.use('/api/statistics', require('./routes/statistics.routes'));
app.use('/api/prueba', require('./routes/prueba.routes'));
app.use('/api/grafos', require('../graphsByDay/routes/grafos.routes'));
app.use('/api/course-stadistics', require('./routes/course-stadistics.routes'));
app.use('/api/login', require('./routes/login.routes'));
app.use('/api/sospechosos', require('./routes/sospechosos.routes'));

app.use('/files', express.static(__dirname + '/assetsPublic'));

app.get(/.*/, function (req, res, next) {
  res.setHeader('Content-Type', 'text/html');
  res.sendFile(__dirname + '/public/index.html');
});

app.listen(app.get('port'), () => {
  console.log('server on port:', app.get('port'))
})