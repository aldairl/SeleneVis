const leanStartup2002IModel = require('../models/leanStartup2020I.model');
const seguimientoModel = require('../models/seguimiento');

class CourseService {

    async createDataCourseLean() {

        console.log("starting to save");
        const dataCourse = await seguimientoModel.find({ course: 'Unicauca+LeanStartUp+2020-I' });

        for (let indexData = 0; indexData < dataCourse.length; indexData++) {
            const register = dataCourse[indexData];
            const registerTosave = new leanStartup2002IModel(register.toJSON());
            await registerTosave.save();
        }

        console.log("update finish")
    }

    async execTaskData() {
        exectTask(this.createDataCourseLean());
    }

}


function exectTask(task) {
    setTimeout(function () { task }, 2000);
}

module.exports = new CourseService();