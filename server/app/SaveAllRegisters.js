const seguimiento = require('../models/seguimiento');
const statics = require('./GetStatics');
const indicatorController = require('../controllers/indicators.controller');
// const activitiesTimeLineModel = require('../models/activitiesTimeLine.model');
// const TimeLineDataCourse = require('../../activitiesTimeLineByCourse/util/TimeLineCourses');

const grafosController = require('../../graphsByDay/controllers/grafos.controller');

const SaveInfoDB = {};
let staticsToSave = {};

SaveInfoDB.saveInfo = async () => {

    // get all courses :P
    const courses = await seguimiento.distinct("course");

    let docsInserteds = 0;
    let totalStudents = 0;

    console.log("updating...");
    courses.forEach(async (course) => {

        const students = await seguimiento.find({ course: course }).distinct('username');
        totalStudents += students.length;
        
        console.log(course);
        students.forEach(async (student) => {

            staticsToSave = await statics.getStatistics({ course: course, student: student })

            // intent save document
            await indicatorController.createIndicator(staticsToSave)
                .then(res => {
                    docsInserteds++;
                    console.log(res, docsInserteds, totalStudents)
                })
                .catch((err) => {
                    console.log("ocurrio un problema guardando el doc", err)
                })

            if (docsInserteds === totalStudents) {
                console.log(`updated ${docsInserteds} documents`)
            }
        })

    })

}

// SaveInfoDB.saveAtivitiesTimeLineCourse = async () => {

//     const TimeLineDataCourseClass = new TimeLineDataCourse();
//     const courses = await seguimiento.distinct("course");

//     console.log("starting save data, number of courses", courses.length)

//     if (courses.length) {

//         let coursesSaved = 0;

//         for (const course of courses) {

//             const days = await seguimiento.find({ "course": course }).distinct('date')

//             if (days.length) {
//                 let generalData = [];
//                 let count = 0;

//                 for (const day of days) {

//                     Promise.all([
//                         TimeLineDataCourseClass.numVideosByCourseDate(course, day),
//                         TimeLineDataCourseClass.numContentByCourseDate(course, day),
//                         TimeLineDataCourseClass.numForosByCourseDate(course, day),
//                         TimeLineDataCourseClass.numExamenByCourseDate(course, day)
//                     ]).then(async (result) => {

//                         generalData.push(result[0], result[1], result[2], result[3]);
//                         count++;

//                         if (count === days.length) {

//                             const conditions = { course }
//                                 , update = { timelinedata: generalData }
//                                 , options = { multi: true, upsert: true };
//                             await activitiesTimeLineModel.updateOne(conditions, update, options);

//                             coursesSaved += 1;
//                             console.log('Fisnish', course, coursesSaved);
//                             // resolve(generalData);
//                             if (coursesSaved == courses.length) {
//                                 console.log("saved all courses")
//                             }
//                         }
//                     })
//                 }

//             }
//         }
//     } else {
//         console.log("not exist courses");
//     }
// }


// SaveInfoDB.saveAllGraphsByDay = async () => {

//     // get all courses :P
//     const courses = await seguimiento.distinct("course");

//     let docsInserteds = 0;
//     let totalStudents = 0;

//     console.log("updating grafos...");

//     for await (let course of courses) {
//         const students = await seguimiento.find({ course: course }).distinct('username');

//         for await (let student of students) {
//             const body = { student, course }

//             const grafoToSave = await grafosController.getGrafosStudentByDay(body);
//             const saved = await grafosController.createGrafo({ ...grafoToSave, control: "day" });

//             console.log(saved);
//         }
//     }

//     console.log("update finish")
// }

// SaveInfoDB.saveAllGraphsBySession = async () => {

//     // get all courses :P
//     const courses = await seguimiento.distinct("course");

//     let docsInserteds = 0;
//     let totalStudents = 0;

//     console.log("updating grafos session...");

//     for await (let course of courses) {
//         const students = await seguimiento.find({ course: course }).distinct('username');

//         for await (let student of students) {
//             const body = { student, course }

//             const grafoToSave = await grafosController.getGrafosStudentBySession(body);
//             const saved = await grafosController.createGrafo({ ...grafoToSave, control: "session" });

//             console.log(saved);
//         }
//     }

//     console.log("update finish")
// }

module.exports = SaveInfoDB;