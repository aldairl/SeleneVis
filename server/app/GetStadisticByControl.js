const seguimiento = require('../models/seguimiento')
const stadisticsBycontrol = {};
const stadisticmodel = require('../models/stadistics');

stadisticsBycontrol.getStadisticsDay = async (req, res) => {
    const course = req.body ? req.body.course : req.course;
    const student = req.body ? req.body.student : req.student;
    const day = req.body.control;

    let dataByDays = [];

    if (!day) { //if not day then send first day information  plus all days for futures consults


        const stadistic = await stadisticmodel.find({ student, course, controlDS: 'day' });

        if (stadistic.length) {

            res.json(stadistic);

        } else {

            const days = await seguimiento.find({ "username": student, "course": course }).distinct('date');

            if (days.length) {

                getBasicStadisticByDay(course, student, days[0]).then(data => {
                    dataByDays.push({ control: days[0], data, days })
                    res.json(dataByDays)
                })

            } else {
                res.json({ msg: "no hay dias" })
            }
        }

    } else {
        // get data by day

        const stadistic = await stadisticmodel.find({ student, course, control: day });

        if (stadistic.length) {

            res.json(stadistic);

        } else {

            getBasicStadisticByDay(course, student, day).then(data => {
                dataByDays.push({ control: day, data });
                res.json(dataByDays);
            })
        }

    }

}

stadisticsBycontrol.getStadisticSession = async (req, res) => {
    const course = req.body ? req.body.course : req.course;
    const student = req.body ? req.body.student : req.student;
    const session = req.body.control;

    let dataBySession = [];

    if (!session) {

        const stadistic = await stadisticmodel.find({ student, course, controlDS: 'session' });

        if (stadistic.length) {

            res.json(stadistic)
        } else {
            const sessions = await seguimiento.find({ "course": course, "username": student }).distinct('session');
            if (sessions.length) {

                getBasicStadisticSession(course, student, sessions[0]).then(data => {
                    dataBySession.push({ control: sessions[0], data, sessions })
                    res.json(dataBySession)
                })

            } else {
                res.json({})
            }
        }

    } else {

        const stadistic = await stadisticmodel.find({ student, course, control: session });

        if (stadistic.length) {
            res.json(stadistic)
        } else {

            getBasicStadisticSession(course, student, session).then(data => {
                dataBySession.push({ control: session, data })
                res.json(dataBySession)
            })
        }

    }

}

//  ===== By Day =====
function getBasicStadisticByDay(course, student, day) {

    return new Promise(async (resolve, reject) => {

        const Videos = await seguimiento.find(
            {
                $and: [
                    { "username": student, "course": course, "date": day },
                    { $or: [{ name: "play_video" }, { name: "pause_video" }, { name: "stop_video" }] }
                ]
            }).countDocuments()


        const Contenido = await seguimiento.find({
            $and: [
                { $or: [{ name: "nav_content" }, { name: "nav_content_click" }, { name: "nav_content_prev" }, { name: "nav_content_next" }, { name: "nav_content_tab" }] },
                { "username": student, "course": course, "date": day }
            ]
        }).countDocuments();

        const Foros = await seguimiento.find({
            $and: [
                { $or: [{ name: "edx.forum.comment.created" }, { name: "edx.forum.response.created" }, { name: "edx.forum.thread.created" }] },
                { "username": student, "course": course, "date": day }
            ]
        }).countDocuments();

        const Examenes = await seguimiento.find({
            $and: [
                // , { name: "problem_graded" }
                { $or: [{ name: "problem_check" }] },
                { "username": student, "course": course, "date": day }
            ]
        }).countDocuments();

        const Home = await seguimiento.find({
            $and: [
                { $or: [{ name: "Signin" }] },
                { "username": student, "course": course, "date": day }
            ]
        }).countDocuments();

        resolve({ Contenido, Videos, Home, Foros, Examenes })
    })
}

// ===== By Session =====
function getBasicStadisticSession(course, student, session) {

    return new Promise(async (resolve, reject) => {

        const Videos = await seguimiento.find(
            {
                $and: [
                    { "username": student, "course": course, "session": session },
                    { $or: [{ name: "play_video" }, { name: "pause_video" }, { name: "stop_video" }] }
                ]
            }).countDocuments()


        const Contenido = await seguimiento.find({
            $and: [
                { $or: [{ name: "nav_content" }, { name: "nav_content_click" }, { name: "nav_content_prev" }, { name: "nav_content_next" }, { name: "nav_content_tab" }] },
                { "username": student, "course": course, "session": session }
            ]
        }).countDocuments();

        const Foros = await seguimiento.find({
            $and: [
                { $or: [{ name: "edx.forum.comment.created" }, { name: "edx.forum.response.created" }, { name: "edx.forum.thread.created" }] },
                { "username": student, "course": course, "session": session }
            ]
        }).countDocuments();

        const Examenes = await seguimiento.find({
            $and: [
                // , { name: "problem_graded" }
                { $or: [{ name: "problem_check" }] },
                { "username": student, "course": course, "session": session }
            ]
        }).countDocuments();

        const Home = await seguimiento.find({
            $and: [
                { $or: [{ name: "Signin" }] },
                { "username": student, "course": course, "session": session }
            ]
        }).countDocuments();

        resolve({ Contenido, Videos, Home, Foros, Examenes })
    })

}


module.exports = stadisticsBycontrol;